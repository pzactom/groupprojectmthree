package runnable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Mock{
	private static Logger logger = LoggerFactory.getLogger(Mock.class);
	public static void show(String out){
		logger.info(Thread.currentThread().getName()+":"+out);
	}
}