package runnable;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Random;

import javax.net.ServerSocketFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import OrderManager.Order;
import OrderManager.OrderManager;
import OrderRouter.Router;
import Ref.Instrument;
import Ref.Ric;

public class SampleRouter extends Thread implements Router {
	private static final Random RANDOM_NUM_GENERATOR = new Random();
	private static final Instrument[] INSTRUMENTS = { new Instrument(new Ric("VOD.L")), new Instrument(new Ric("BP.L")),
			new Instrument(new Ric("BT.L")) };
	private Socket omConn;
	private int port;
	private Logger logger = LoggerFactory.getLogger(SampleRouter.class);
	private ObjectInputStream is;
	private ObjectOutputStream os;

	
	public SampleRouter(String name, int port) {
		this.setName(name);
		this.port = port;
	}
	
	public void run() {
		// OM will connect to us
		try {
			omConn = ServerSocketFactory.getDefault().createServerSocket(port).accept();
			while (true) {
				if (0 < omConn.getInputStream().available()) {
					is = new ObjectInputStream(omConn.getInputStream());
					Router.api methodName = (Router.api) is.readObject();
					logger.info("Recieved method call for: {}", methodName);
					switch (methodName) {
					case routeOrder:
						routeOrder(is.readInt(), is.readInt(), is.readInt(), (Instrument) is.readObject());
						break;
					case priceAtSize:
						priceAtSize(is.readInt(), is.readInt(), (Instrument) is.readObject(), is.readInt());
						break;
					case sendCancel:
						int orderId = is.readInt();
						Order o = (Order) is.readObject();
						cancelOrder(orderId);
					}
				} else {
					Thread.sleep(100);
				}
			}
		} catch (IOException | ClassNotFoundException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void routeOrder(int id, int sliceId, int size, Instrument i) throws IOException, InterruptedException { // MockI.show(""+order);
		int fillSize = RANDOM_NUM_GENERATOR.nextInt(size);
		// TODO have this similar to the market price of the instrument
		double fillPrice = SampleLiveMarketData.getLivePrice(i.getRic()) + ((RANDOM_NUM_GENERATOR.nextDouble() * 10) - 5);
		Thread.sleep(42);
		logger.debug("Routing order {}, slice id {}, maxSize {}, sizeFilled {}, for insrument {}", id, sliceId, size, fillSize, i);
		os = new ObjectOutputStream(omConn.getOutputStream());
		os.writeObject(OrderManager.api.newFill);
		os.writeInt(id);
		os.writeInt(sliceId);
		os.writeInt(fillSize);
		os.writeDouble(fillPrice);
		os.flush();
	}


	@Override
	public void priceAtSize(int id, int sliceId, Instrument i, int size) throws IOException {
		os = new ObjectOutputStream(omConn.getOutputStream());
		os.writeObject(OrderManager.api.bestPrice);
		os.writeInt(id);
		os.writeInt(sliceId);
		os.writeDouble(SampleLiveMarketData.getLivePrice(i.getRic()) + ((RANDOM_NUM_GENERATOR.nextDouble() * 10) - 5));
		os.flush();
	}

	public void cancelOrder(int orderID) {
		logger.info("Canceled order {}", orderID);
	}
	
	@Override
	public void sendCancel(int id, Instrument i, int sliceId, int size) {
		// TODO Auto-generated method stub
	}
}
