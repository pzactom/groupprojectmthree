package runnable;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;

import org.slf4j.LoggerFactory;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;

import OrderManager.OrderManager;
import tests.OrderManager.LiveMarketDataDummy;

public class Main{
	
	static final Logger logger = LoggerFactory.getLogger(Main.class);
	static Configurations configs = new Configurations();
	
	public static void main(String[] args) throws IOException, ConfigurationException{
		logger.info("This program tests the OrderManager");

		Configuration config = configs.properties(new File("sample.properties"));
		//Load the config data
		Integer[] clientPorts = (Integer[])config.getArray(Integer.class, "client.port");
		String[] clientNames = config.getStringArray("client.name");
		String[] clientHost = config.getStringArray("client.host");
		
		Integer[] routerPorts = (Integer[])config.getArray(Integer.class, "router.port");
		String[] routerNames = config.getStringArray("router.name");
		String[] routerHost = config.getStringArray("router.host");
		
		String traderName = config.getString("trader.name");
		Integer traderPort = config.getInt("trader.port");
		String traderHost = config.getString("trader.host");
		
		InetSocketAddress[] clients = new InetSocketAddress[clientPorts.length];
		InetSocketAddress[] routers = new InetSocketAddress[routerPorts.length];
		
		LiveMarketDataDummy lmd = new LiveMarketDataDummy(2021);
		new Thread(lmd).start();
		
		for(int i = 0; i < clientPorts.length; i++) {
			(new MockClient(clientNames[i], clientPorts[i])).start();
			clients[i] = new InetSocketAddress(clientHost[i], clientPorts[i]);
		}
		
		for(int i = 0; i< routerPorts.length; i++) {
			(new SampleRouter(routerNames[i], routerPorts[i])).start();
			routers[i] = new InetSocketAddress(routerHost[i], routerPorts[i]);
		}
		
		(new Trader(traderName, traderPort)).start();
		InetSocketAddress trader=new InetSocketAddress(traderHost, traderPort);

	
		InetSocketAddress liveMarketData= new InetSocketAddress("localhost",2021);
		(new MockOM("Order Manager",routers,clients,trader,liveMarketData)).start();//placed all the above
	}
}

class MockClient extends Thread{//send an order and wait message to come back
	private Logger logger = LoggerFactory.getLogger(MockClient.class);
	
	int port;
	MockClient(String name,int port){
		this.port=port;
		this.setName(name);
	}
	public void run(){
		try {
			SampleClient client=new SampleClient(port);
			if(port==2000){
				//this takes an argument because it implements Client
				client.sendOrder(null);
				int id=client.sendOrder(null);
				client.sendCancel(id);
				client.messageHandler();
			}else{
				client.sendOrder(null);
				client.messageHandler();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}

class MockOM extends Thread{
	
	Logger logger = LoggerFactory.getLogger(Main.class);
	
	InetSocketAddress[] clients;
	InetSocketAddress[] routers;
	InetSocketAddress trader;
	InetSocketAddress liveMarketData;

	MockOM(String name,InetSocketAddress[] routers,InetSocketAddress[] clients,InetSocketAddress trader,InetSocketAddress liveMarketData){
		this.clients=clients;
		this.routers=routers;
		this.trader=trader;
		this.liveMarketData=liveMarketData;
		this.setName(name);
	}
	@Override
	public void run(){
		try{
			//In order to debug constructors you can do F5 F7 F5
			new OrderManager(routers,clients,trader,liveMarketData);
		}catch(IOException | ClassNotFoundException | InterruptedException ex){
			logger.error("An exception occured with the order manager: ", ex);
		}
	}
}