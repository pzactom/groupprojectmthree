package runnable;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Random;

import javax.net.ServerSocketFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import LiveMarketData.LiveMarketData;
import OrderManager.Order;
import Ref.Ric;

public class SampleLiveMarketData implements LiveMarketData, Runnable {
	private static final Random RANDOM_NUM_GENERATOR = new Random();
	private Socket omConn;
	private int port;
	private static String url = "https://uk.reuters.com/business/stocks/overview/";
	private static Logger logger = LoggerFactory.getLogger(SampleLiveMarketData.class);

	SampleLiveMarketData(int port) {
		this.port = port;
	}
	
	/**
	 * Connecting through socket to Live Market Data
	 */
	@Override
	public void run() {
		try {
			omConn = ServerSocketFactory.getDefault().createServerSocket(port).accept();
			ObjectInputStream is = new ObjectInputStream(omConn.getInputStream());
			while (true) {
				if (0 < is.available()) {
					Order o = (Order) is.readObject();
					o.initialMarketPrice = getLivePrice(o.instrument.getRic());
					ObjectOutputStream os = new ObjectOutputStream(omConn.getOutputStream());
					os.writeObject(o);
					os.flush();
				} else {
					Thread.sleep(1000);
				}
			}
		} catch (IOException | InterruptedException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Get the live price data from the given URL. This method will only work with uk.reuters.com, any other site
	 * will not work due to the simple nature of the fetch.
	 * @param o - The order to get the initial market price for
	 * @return The initial market price, if non found then a random value.
	 */
	public static double getLivePrice(Ric r) {
		try {
			URL request = new URL(url + r.ric);
			logger.debug("Getting market price from : {}", request);
			BufferedReader in = new BufferedReader(new InputStreamReader(request.openStream()));
			
			String line = null;
			while((line = in.readLine()) != null) {
				//System.out.println(line);
				if(line.contains("nasdaqChangeHeader")) {
					//Read 3 more lines, 2 of them are line breaks, the third is a span that contains the data.
					in.readLine();
					in.readLine();
					in.readLine();
					line = in.readLine();
					line = line.substring(0,line.indexOf('<')).trim();
					return Double.parseDouble(line);
				}
			}
			throw new Exception("nasdaqChangeHeader not found");
			
		} catch (Exception e) {
			logger.error("Failed to get price! Using random number", e);
			return 199 * RANDOM_NUM_GENERATOR.nextDouble(); 
		}
	}
}
