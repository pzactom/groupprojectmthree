package runnable;

import org.slf4j.LoggerFactory;

import org.slf4j.Logger;

public class MockI{
	private static Logger logger = LoggerFactory.getLogger(MockI.class);
	
	public static void show(String out){
		logger.info("{}", out);
	}
}