package runnable;

import java.io.BufferedInputStream;
import java.io.DataInput;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import javax.net.ServerSocketFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import OrderManager.Order;
import OrderManager.OrderManager;
import TradeScreen.TradeScreen;

public class Trader extends Thread implements TradeScreen {
	private HashMap<Integer, Order> orders = new HashMap<Integer, Order>();
	private HashMap<Integer, Order> cancelledOrders = new HashMap<Integer, Order>();
	private Socket omConn;
	private int port;
	private Logger logger = LoggerFactory.getLogger(Trader.class);
	ObjectInputStream is = null;
	ObjectOutputStream os;

	Trader(String name, int port) {
		this.setName(name);
		this.port = port;
	}

	public void run() {
		// OM will connect to us
		try {
			omConn = ServerSocketFactory.getDefault().createServerSocket(port).accept();

			// is=new ObjectInputStream( omConn.getInputStream());
			InputStream s = omConn.getInputStream(); // if i try to create an objectinputstream before we have data it
														// will block
			while (true) {
				if (0 < s.available()) {

					is = new ObjectInputStream(s);

					/*
					 * we need to create each time, because InputStream does not support any way to
					 * reset it. This will block if no data, but maybe we can still try to create it
					 * once instead of repeatedly
					 */

					api method = (api) is.readObject();
					logger.debug("calling: {}", method);
					switch (method) {
					case newOrder:
						newOrder((is).readInt(), (Order) is.readObject());
						break;
					case price:
						price(is.readInt(), (Order) is.readObject());
						break;
					case cross:
						cross(is.readInt(), (Order) is.readObject());
						break;
					case fill:
						fill(is.readInt(), (Order) is.readObject());
						break;
					case cancel:
						cancelledHandleOrder(is.readInt(), (Order) is.readObject());
						break;
					case complete:
						completeHandleOrder(is.readInt(), (Order) is.readObject());
						break;
					}
				} else {
					Thread.sleep(1000);
				}
			}
		} catch (IOException | ClassNotFoundException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 *  update orders
	 */
	@Override
	public void newOrder(int id, Order order) throws IOException, InterruptedException {
		Thread.sleep(2134);
		//check if the order has already been canceled
		if(cancelledOrders.containsKey(id)) {
			logger.debug("Cannot accept a cancelled order {}, ClientID, {}, ClientOrderID {}", id, order.clientid, order.ClientOrderID);
			return;
		}
		orders.put(id, order);
		acceptOrder(id);
	}

	@Override
	public void acceptOrder(int id) throws IOException {
		os = new ObjectOutputStream(omConn.getOutputStream());
		os.writeObject(OrderManager.api.acceptOrder);
		os.writeInt(id);
		os.flush();
	}

	@Override
	public void sliceOrder(int id, int sliceSize) throws IOException {
		os = new ObjectOutputStream(omConn.getOutputStream());
		os.writeObject(OrderManager.api.sliceOrder);
		os.writeInt(id);
		os.writeInt(sliceSize);
		os.flush();
	}

	/**
	 *  updates order and slices order
	 */
	@Override
	public void price(int id, Order o) throws InterruptedException, IOException {
		orders.put(id, o);
		Thread.sleep(2134);
		sliceOrder(id, orders.get(id).sizeRemaining() / 2);
	}

	/**
	 *  updates logger and order
	 */
	@Override
	public void cross(int id, Order matchingOrder) throws IOException {
		logger.info("Successful cross acknowledged: ID: " + id);
		orders.put(id, matchingOrder);
	}

	/**
	 *  updates logger and order
	 */
	@Override
	public void fill(int id, Order matchingOrder) throws IOException {
		logger.info("Successful fill acknowledged: ID: " + id);
		orders.put(id, matchingOrder);
	}
	
	public void cancelOrder(int id) throws IOException {
		os = new ObjectOutputStream(omConn.getOutputStream());
		os.writeObject(OrderManager.api.cancelOrder);
		os.writeInt(id);
		os.flush();
	}

	
	/**
	 * @return a list with all the Accepted orders
	 */
	public ArrayList <Order> getAccepted() {
		ArrayList <Order> orderList = new ArrayList <Order>();
		for(Order o: orders.values()) {
			if (o.ordStatus == '0') {
				orderList.add(o);
			}
		}
		return orderList;
	}
	
	/**
	 * @return a list with all the Cancelled orders
	 */
	public ArrayList<Order> getCancelled() {
		ArrayList <Order> orderList = new ArrayList <Order>();
		for(Order o: cancelledOrders.values()) {
			if (o.ordStatus == '4') {
				orderList.add(o);
			}
		}
		return orderList;
	}

	/**
	 * @return a list with all the Pending orders
	 */
	public ArrayList<Order> getPending() {
		ArrayList <Order> orderList = new ArrayList <Order>();
		for(Order o: orders.values()) {
			if (o.ordStatus == 'A') {
				orderList.add(o);
			}
		}
		return orderList;
	}


	/**
	 * @return a list with all the Partially Filled orders
	 */
	public ArrayList<Order> getPartiallyFilled() {
		ArrayList <Order> orderList = new ArrayList <Order>();
		for(Order o: orders.values()) {
			if (o.ordStatus == '1') {
				orderList.add(o);
			}
		}
		return orderList;
	}

	/**
	 * @return a list with all the Filled orders
	 */
	public ArrayList<Order> getFilled() {
		ArrayList <Order> orderList = new ArrayList <Order>();
		for(Order o: orders.values()) {
			if (o.ordStatus == '2') {
				orderList.add(o);
			}
		}
		return orderList;
	}
	
	/**
	 * Handle the cancelling of an order. The order will be removed unless its completed and then added to
	 * the cancelled order hashmap
	 * @param id - The OM id for the order
	 * @param order - The cancelled order
	 */
	private void cancelledHandleOrder(int id, Order order) {
		logger.info("Order {} confimed cancellation", id);
		//If the order is not compete then remove it from the list
		if(orders.get(id).ordStatus != '2') {
			orders.remove(id);
		}
		
		//Add the order to cancelled
		cancelledOrders.put(id, order);
	}
	
	private void completeHandleOrder(int id, Order order) {
		logger.info("Order {} confirmed complete", id);
		orders.put(id, order);
	}

}
