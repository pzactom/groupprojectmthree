package runnable;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import OrderClient.Client;
import OrderClient.NewOrderSingle;
import OrderManager.Order;
import OrderManager.OrderManager;
import Ref.Instrument;
import Ref.Ric;

public class SampleClient extends Mock implements Client{//interface of
	private static final Random RANDOM_NUM_GENERATOR=new Random();
	private static final Instrument[] INSTRUMENTS={new Instrument(new Ric("VOD.L")), new Instrument(new Ric("BP.L")), new Instrument(new Ric("BT.L"))};
	private static final HashMap OUT_QUEUE=new HashMap(); //queue for outgoing orders//hashmap store ll the orders ro the manager
	private int id=0; //message id number
	private Socket omConn; //connection to order manager
	private Logger logger = LoggerFactory.getLogger(SampleClient.class);
			
	public SampleClient(int port) throws IOException{
		//OM will connect to us
		omConn=new ServerSocket(port).accept();//listen to the port
		logger.info("Client awaiting connection to OrderManager on port {}", port);
	}
	
	@Override
	public int sendOrder(Object par0)throws IOException{
		int size=RANDOM_NUM_GENERATOR.nextInt(5000);//generate order rndom size nd send it
		int instid=RANDOM_NUM_GENERATOR.nextInt(3);
		Instrument instrument=INSTRUMENTS[RANDOM_NUM_GENERATOR.nextInt(INSTRUMENTS.length)];
		boolean buy = RANDOM_NUM_GENERATOR.nextBoolean();
		NewOrderSingle nos=new NewOrderSingle(size,instid,instrument, buy);
		
		show("sendOrder: id="+id+" size="+size+" instrument="+INSTRUMENTS[instid].toString());
		OUT_QUEUE.put(id,nos);
		if(omConn.isConnected()){//if connect to order manager try to send message if not 
			ObjectOutputStream os=new ObjectOutputStream(omConn.getOutputStream());
			os.writeObject(OrderManager.api.newOrderSingle);
			//os.writeObject("35=D;");
			os.writeInt(id);//id order
			os.writeObject(nos);
			os.flush();
		}
		return id++;//get different id order
	}


	@Override
	public void partialFill(Order order){
		logger.info("Order partial fill: {}", order);
	}

	@Override
	public void fullyFilled(Order order){
		logger.info("Order fully filled: {}", order);
		OUT_QUEUE.remove(order.ClientOrderID);//when order place remove from queue
	}

	@Override
	public void cancelled(Order order){
		logger.info("Order canceled: {}", order);
		OUT_QUEUE.remove(order.ClientOrderID);
	}
	
	

	enum methods{newOrderSingleAcknowledgement,dontKnow};
	@Override
	public void messageHandler(){
		
		ObjectInputStream is;
		try {
			while(true){
				//is.wait(); //this throws an exception!!
				while(0<omConn.getInputStream().available()){
					is = new ObjectInputStream(omConn.getInputStream());
					String fix=(String)is.readObject();//fix= message
					logger.debug("recevied fix message: " + fix);
					String[] fixTags=fix.split(";");
					int OrderId=-1;
					char MsgType;
					int OrdStatus;
					methods whatToDo=methods.dontKnow;
					//String[][] fixTagsValues=new String[fixTags.length][2];
					for(int i=0;i<fixTags.length;i++){
						String[] tag_value=fixTags[i].split("=");
						switch(tag_value[0]){
							case"11":OrderId=Integer.parseInt(tag_value[1]);break;
							case"35":MsgType=tag_value[1].charAt(0);
								if(MsgType=='A')whatToDo=methods.newOrderSingleAcknowledgement;
								break;
							case"39":OrdStatus=tag_value[1].charAt(0);break;
						}
					}
					switch(whatToDo){
						case newOrderSingleAcknowledgement:newOrderSingleAcknowledgement(OrderId);
					}
					
					/*message=connection.getMessage();
					char type;
					switch(type){
						case 'C':cancelled(message);break;
						case 'P':partialFill(message);break;
						case 'F':fullyFilled(message);
					}*/
				}
			}
		} catch (IOException|ClassNotFoundException e){
			logger.error("", e);
		}
	}

	void newOrderSingleAcknowledgement(int OrderId){
		logger.info(Thread.currentThread().getName()+" called newOrderSingleAcknowledgement");
		//do nothing, as not recording so much state in the NOS class at present
	}
/*listen for connections
once order manager has connected, then send and cancel orders randomly
listen for messages from order manager and print them to stdout.*/

	@Override
	public void sendCancel(int id) {
		try {
			logger.info("Sending request to cancel order ClientOrderID {}", id);
			ObjectOutputStream os=new ObjectOutputStream(omConn.getOutputStream());
			os.writeObject(OrderManager.api.cancelOrder);
			os.writeInt(id);//id order
			os.flush();
		} catch(Exception e) {
			logger.error("Failed to cancel order", e);
		}
	}
}