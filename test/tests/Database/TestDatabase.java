package tests.Database;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

import Database.Database;

public class TestDatabase {
	
	private final ByteArrayOutputStream output = new ByteArrayOutputStream();
	private BufferedReader readableOut = null;
	String line;

	/**
	 * Change output from displaying at the screen to a Stream
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		System.setOut(new PrintStream(output));
		
	}

	/**
	 * Tests if Database.write can print correctly Objects of type String
	 */
	@Test
	public void testString() {
		Database.write("Hello!");
		ByteArrayInputStream input = new ByteArrayInputStream(output.toByteArray());
		readableOut = new BufferedReader(new InputStreamReader(input));
		try {
			line = readableOut.readLine();
		assertEquals("Hello!", line);
		} catch (IOException e) {
			e.printStackTrace();
		}
}
	
	/**
	 * Tests if Database.write can print correctly Objects of type Integer
	 */
	@Test
	public void testInteger() {
		Database.write(123456);
		ByteArrayInputStream input = new ByteArrayInputStream(output.toByteArray());
		readableOut = new BufferedReader(new InputStreamReader(input));
		try {
			line = readableOut.readLine();
			Integer random = 123456;
		assertEquals(random.toString(), line);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Tests if Database.write can print correctly Objects of type Boolean
	 */
	@Test
	public void testBoolean() {
		Database.write(true);
		ByteArrayInputStream input = new ByteArrayInputStream(output.toByteArray());
		readableOut = new BufferedReader(new InputStreamReader(input));
		try {
			line = readableOut.readLine();
			Boolean tmp = true;
		assertEquals(tmp.toString(), line);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
