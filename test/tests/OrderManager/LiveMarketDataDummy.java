package tests.OrderManager;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import javax.net.ServerSocketFactory;

import LiveMarketData.LiveMarketData;
import OrderManager.Order;

public class LiveMarketDataDummy implements LiveMarketData, Runnable {
	private int port;
	private Socket omConn;

	public LiveMarketDataDummy(int port) {
		this.port = port;
	}
	
	/**
	 * Connecting through socket to Live Market Data
	 */
	@Override
	public void run() {
		try {
			
			omConn = ServerSocketFactory.getDefault().createServerSocket(port).accept();
			
			while (true) {
				if (0 < omConn.getInputStream().available()) {
					ObjectInputStream is = new ObjectInputStream(omConn.getInputStream());
					Order o = (Order) is.readObject();
					o.initialMarketPrice = 10;
					ObjectOutputStream os = new ObjectOutputStream(omConn.getOutputStream());
					os.writeObject(o);
					os.flush();
				} else {
					Thread.sleep(1000);
				}
			}
		} catch (IOException | InterruptedException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
