package tests.OrderManager;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.LoggerFactory;

import OrderClient.NewOrderSingle;
import OrderManager.Order;
import OrderManager.OrderManager;
import Ref.Instrument;
import Ref.Ric;
import TradeScreen.TradeScreen;
import runnable.Trader;

public class TestOrderManager {
	
	static InetSocketAddress[] clientInets={new InetSocketAddress("localhost",2000),
            new InetSocketAddress("localhost",2001)};
	
	static InetSocketAddress[] routersInets={new InetSocketAddress("localhost",2010),
            new InetSocketAddress("localhost",2011)};
	
	static InetSocketAddress traderInets=new InetSocketAddress("localhost",2020);
	static InetSocketAddress liveMarketData=new InetSocketAddress("localhost",2021);
	static LiveMarketDataDummy lmd = new LiveMarketDataDummy(2021);
	
	private final Instrument[] INSTRUMENTS={new Instrument(new Ric("VOD.L")), new Instrument(new Ric("BP.L")), new Instrument(new Ric("BT.L"))};
	
	static DummyServer client1, client2, router1, router2, trader;
	static MockOM orderMan;
	static int lastOrderID = 0;
	
	@BeforeClass
	public static void setUp() throws Exception {
		client1 = new DummyServer(2000);
		client2 = new DummyServer(2001);
		router1 = new DummyServer(2010);
		router2 = new DummyServer(2011);
		trader = new DummyServer(2020);
		client1.start();
		client2.start();
		router1.start();
		router2.start();
		trader.start();
		new Thread(lmd).start();
		
		orderMan = new MockOM("TestOM", routersInets, clientInets, traderInets, liveMarketData);
		orderMan.start();
		Thread.sleep(100);
	}
	
	@AfterClass
	public static void cleanUp() throws Exception {
		client1.close();
		client2.close();
		router1.close();
		router2.close();
		trader.close();
		client1.join();
		client2.join();
		router1.join();
		router2.join();
		trader.join();
	}

	/**
	 * Test that each of the required connections have been establshed by the order manager.
	 */
	@Test
	public void testConnction() {
		assertTrue("Checking if client 1 has connected", client1.isConnected());
		assertTrue("Checking if client 2 has connected", client2.isConnected());
		assertTrue("Checking if router 1 has connected", router1.isConnected());
		assertTrue("Checking if router 2 has connected", router2.isConnected());
		assertTrue("Checking if trader has connected", trader.isConnected());
		
	}
	
	/**
	 * Test that when a new order is created that the order manager confirms the order by sneding
	 * back the expected response. The test sends the following to the order manager:
	 * method - String
	 * clientOrderID - Integer
	 * nos - NewOrderObject
	 * @throws InterruptedException
	 * @throws IOException
	 */
	@Test
	public void testNewBuyOrder() throws InterruptedException, IOException {
		try {
			//Create the order and send it to the manager
			NewOrderSingle nos = new NewOrderSingle(100, 100.23f, INSTRUMENTS[0], true);
			ObjectOutputStream os = client1.getObjectOutputStream();
			
			os.writeObject("newOrderSingle");
			os.writeInt(lastOrderID);
			os.writeObject(nos);
			
			//Wait for the manager response
			ObjectInputStream clientIS = client1.getObjetInputStream();
			String reply = (String)clientIS.readObject();
			assertTrue(reply.contains("35=A;39=A;"));
			
			//Confirm that the trader also received the message
			ObjectInputStream traderIS = trader.getObjetInputStream();
			TradeScreen.api method = (TradeScreen.api)traderIS.readObject();
			int id = traderIS.readInt();
			Order order = (Order)traderIS.readObject();			
			
			assertEquals(TradeScreen.api.newOrder, method);
			assertTrue(order != null);
			lastOrderID++;
		} catch (Exception e) {
			e.printStackTrace();
			fail("An exception occured");
		}
	}
	
	/**
	 * Test that when a new order is created that the order manager confirms the order by sneding
	 * back the expected response. The test sends the following to the order manager:
	 * method - String
	 * clientOrderID - Integer
	 * nos - NewOrderObject
	 * @throws InterruptedException
	 * @throws IOException
	 */
	@Test
	public void testNewSellOrder() throws InterruptedException, IOException {
		try {
			//Create the order and send it to the manager
			NewOrderSingle nos = new NewOrderSingle(100, 100.23f, INSTRUMENTS[0], false);
			ObjectOutputStream os = client1.getObjectOutputStream();
			
			os.writeObject("newOrderSingle");
			os.writeInt(lastOrderID);
			os.writeObject(nos);
			
			//Wait for the manager response
			ObjectInputStream clientIS = client1.getObjetInputStream();
			String reply = (String)clientIS.readObject();
			assertTrue(reply.contains("35=A;39=A;"));
			
			//Confirm that the trader also received the message
			ObjectInputStream traderIS = trader.getObjetInputStream();
			TradeScreen.api method = (TradeScreen.api)traderIS.readObject();
			int id = traderIS.readInt();
			Order order = (Order)traderIS.readObject();			
			
			assertEquals(TradeScreen.api.newOrder, method);
			assertTrue(order != null);
			lastOrderID++;
		} catch (Exception e) {
			e.printStackTrace();
			fail("An exception occured");
		}
	}
	
	/**
	 * Send an order to the order manager, then send the FIX to accept the order and await confirmation.
	 * Calls the testNewOrder() method first to ensure an order is present
	 * @see #testNewOrder()
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@Test
	public void acceptBuyOrder() throws InterruptedException, IOException, ClassNotFoundException {
		testNewBuyOrder();
		
		//Tell the order manager the order has been accepted
		ObjectOutputStream traderOS = trader.getObjectOutputStream();
		traderOS.writeObject("acceptOrder");
		traderOS.writeInt(lastOrderID-1);
		traderOS.flush();
		
		//The client will be informed of the confirmation
		ObjectInputStream client1IS = client1.getObjetInputStream();
		String confirmation = (String)client1IS.readObject();
		assertTrue(confirmation.contains("39=0"));
		
		//The trader will be informed of the price
		ObjectInputStream traderIS = trader.getObjetInputStream();
		//TradeScreen.api method = null;
		try {
		Object method = traderIS.readObject();
		System.out.println(method.getClass().getName());
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		int id = traderIS.readInt();
		Order order = (Order)traderIS.readObject();
		//assertEquals(TradeScreen.api.price, method);
	}
	
	/**
	 * Send an order to the order manager, then send the FIX to accept the order and await confirmation.
	 * Calls the testNewOrder() method first to ensure an order is present
	 * @see #testNewOrder()
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@Test
	public void acceptSellOrder() throws InterruptedException, IOException, ClassNotFoundException {
		testNewSellOrder();
		
		//Tell the order manager the order has been accepted
		ObjectOutputStream traderOS = trader.getObjectOutputStream();
		traderOS.writeObject("acceptOrder");
		traderOS.writeInt(lastOrderID-1);
		traderOS.flush();
		
		//The client will be informed of the confirmation
		ObjectInputStream client1IS = client1.getObjetInputStream();
		String confirmation = (String)client1IS.readObject();
		assertTrue(confirmation.contains("39=0"));
		
		//The trader will be informed of the price
		ObjectInputStream traderIS = trader.getObjetInputStream();
		//TradeScreen.api method = null;
		try {
		Object method = traderIS.readObject();
		System.out.println(method.getClass().getName());
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		int id = traderIS.readInt();
		Order order = (Order)traderIS.readObject();
		//assertEquals(TradeScreen.api.price, method);
	}
	
	/**
	 * Submit two new order and accept both, then the last order to be submitted
	 * is split. This will cause an internal cross, once complete the trader will
	 * be notified of the cross.
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 * @throws IOException
	 */
	@Test
	public void sliceOrder() throws ClassNotFoundException, InterruptedException, IOException {
		//Need two orders for matching to take place
		acceptBuyOrder();
		acceptSellOrder();
		
		//Get the trader writer and request the last order is split by 25
		ObjectOutputStream traderOS = trader.getObjectOutputStream();
		traderOS.writeObject("sliceOrder");
		traderOS.writeInt(lastOrderID-1);
		traderOS.writeInt(25);
		traderOS.flush();
		
		//Trade notified of internal cross
		ObjectInputStream traderIS = trader.getObjetInputStream();
		TradeScreen.api method = (TradeScreen.api)traderIS.readObject();
		int id = traderIS.readInt();
		Order order = (Order)traderIS.readObject();
		assertEquals(TradeScreen.api.cross, method);
	}
}

class MockOM extends Thread{
	InetSocketAddress[] clients;
	InetSocketAddress[] routers;
	InetSocketAddress trader;
	InetSocketAddress liveMarketData;
	MockOM(String name,InetSocketAddress[] routers,InetSocketAddress[] clients,InetSocketAddress trader,InetSocketAddress liveMarketData){
		this.clients=clients;
		this.routers=routers;
		this.trader=trader;
		this.liveMarketData=liveMarketData;
		this.setName(name);
	}
	@Override
	public void run(){
		try{
			//In order to debug constructors you can do F5 F7 F5
			new OrderManager(routers,clients,trader,liveMarketData);
		}catch(IOException | ClassNotFoundException | InterruptedException ex){
			Logger.getLogger(MockOM.class.getName()).log(Level.SEVERE,null,ex);//we do not use it if get exception we get logger
		}
	}
}
