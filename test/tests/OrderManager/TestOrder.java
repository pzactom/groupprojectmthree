package tests.OrderManager;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import OrderManager.Order;
import Ref.Instrument;
import Ref.Ric;

public class TestOrder {
	Order o = new Order(0, 5, 15, new Instrument(new Ric("M3.L")), 25, true);

	@Before
	public void setUp() throws Exception {
		o = new Order(1, 5, 15, new Instrument(new Ric("M3.L")), 25, true);
	}

	/**
	 * Checks if when adding a new order to the list, the list indeed grows
	 */
	@Test
	public void TestNewSlice() {
		assertEquals(0, o.newSlice(35));
		assertEquals(1, o.newSlice(25));
	}

	/**
	 * Checks if sliceSizes returns the total size of all slices
	 */
	@Test
	public void TestSliceSizes() {
		assertEquals(0, o.newSlice(35));
		assertEquals(1, o.newSlice(25));
		assertEquals(60, o.sliceSizes());
	}

	/**
	 * Checks if SizeFilled returns the expected number
	 */
	@Test
	public void TestSizeFilled() {
		assertEquals(0, o.sizeFilled());
	}

	/**
	 * Checks if SizeRemaining returns the expected number
	 */
	@Test
	public void TestSizeRemaining() {
		assertEquals(25, o.sizeRemaining());
	}

	/**
	 * Check if cross matches the orders correctly case that first order's total
	 * size is smaller or equal than the second's
	 */
	@Test
	public void TestCross_caseLessEqual() {
		// create first order
		o = new Order(0, 5, 15, new Instrument(new Ric("M3.L")), 250, true);
		o.newSlice(50);
		o.newSlice(25);
		assertEquals(250, o.sizeRemaining());
		assertEquals(75, o.sliceSizes());
		// create second order
		Order o2 = new Order(1, 10, 20, new Instrument(new Ric("N4.L")), 300, false);
		o2.newSlice(40);
		o2.newSlice(20);
		assertEquals(300, o2.sizeRemaining());
		assertEquals(60, o2.sliceSizes());

		o.cross(o2);

		// checks after crossing
		assertEquals(0, o.sizeRemaining());
		assertEquals(250, o.sizeFilled());
		assertEquals(75, o.sliceSizes());

		assertEquals(50, o2.sizeRemaining());
		assertEquals(250, o2.sizeFilled());
		assertEquals(60, o2.sliceSizes());

	}

	/**
	 * Check if cross matches the orders correctly case that first order's total
	 * size is greater than the second's
	 */
	@Test
	public void TestCross_caseMore() {
		// create first order
		o = new Order(0, 5, 15, new Instrument(new Ric("M3.L")), 250, false);
		o.newSlice(50);
		o.newSlice(25);
		assertEquals(250, o.sizeRemaining());
		assertEquals(75, o.sliceSizes());
		// create second order
		Order o2 = new Order(1, 10, 20, new Instrument(new Ric("N4.L")), 300, true);
		o2.newSlice(40);
		o2.newSlice(20);
		assertEquals(300, o2.sizeRemaining());
		assertEquals(60, o2.sliceSizes());

		o2.cross(o);

		// checks after crossing
		assertEquals(0, o.sizeRemaining()); // 10
		assertEquals(250, o.sizeFilled()); // 240 equals o2.sizeFilled as it should
		assertEquals(75, o.sliceSizes());

		assertEquals(50, o2.sizeRemaining()); // 60
		assertEquals(250, o2.sizeFilled()); // 240
		assertEquals(60, o2.sliceSizes());

	}

}