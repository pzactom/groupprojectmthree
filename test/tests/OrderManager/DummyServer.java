package tests.OrderManager;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.net.ServerSocketFactory;

public class DummyServer extends Thread{
	
	private Socket omConnection;
	private int connectionPort;
	
	private ObjectInputStream  is;
	private ObjectOutputStream os;
	
	public DummyServer(int port) {
		connectionPort = port;
	}
	
	public void run() {
		try {
			omConnection = ServerSocketFactory.getDefault().createServerSocket(connectionPort).accept();
			System.out.println(Thread.currentThread().getName() + " connected");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Return true if the dummy server has a connected client
	 * @return True if there is a connected client
	 */
	public boolean isConnected() {
		return omConnection.isConnected();
	}

	/**
	 * Get the ObjectOutputStream to allow then sending of object to the connected client.
	 * If the isConnected is false then this will throw a null pointer aceeption.
	 * @return The ObjectOutputSteam for the connected client
	 */
	public ObjectOutputStream getObjectOutputStream() {
		try {
			os = new ObjectOutputStream(omConnection.getOutputStream());
			return os;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Get the ObjectInputStream to read object being send from the connected client.
	 * If isConencted is false then this method will throw a null pointer acception 
	 * @return The ObjectInputStrean of the connected client
	 */
	public ObjectInputStream getObjetInputStream() {
		try {
			InputStream inputStream = omConnection.getInputStream();
			is = new ObjectInputStream(inputStream);
			return is;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Close the connection to the conneted client and any open streams
	 */
	public void close() {
		try {
			omConnection.close();
			if(is != null) is.close();
			if(os != null) os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
