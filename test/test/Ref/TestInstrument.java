package test.Ref;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Ref.Instrument;
import Ref.Ric;

public class TestInstrument {
	Instrument ins = new Instrument(new Ric("M3.L"));

	/**
	 * Check if an Instrument returns the correct RIC
	 */
	@Test
	public void testToString() {
		assertEquals("M3.L", ins.toString());
	}	

}
