package test.Ref;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Ref.Ric;

public class TestRic {

	/**
	 * Checks if the two parts of the String are correct
	 */
	@Test
	public void testRic() {
		Ric ric = new Ric("M3.L");
		assertEquals("M3", ric.getCompany());
		assertEquals("L", ric.getEx());
	}

}
