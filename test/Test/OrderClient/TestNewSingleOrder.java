package test.OrderClient;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.io.Serializable;

import org.junit.Before;
import org.junit.Test;

import OrderClient.Client;
import OrderClient.NewOrderSingle;
import OrderManager.Order;
import Ref.Instrument;
import Ref.Ric;

public class TestNewSingleOrder {

	private final ByteArrayOutputStream outPut = new ByteArrayOutputStream();
	private BufferedReader br = null;
	 int size;
	 float price;
	 String string = "Yes";
	@Before
	public void setUp() throws Exception {

		System.setOut(new PrintStream(outPut));
	}

	/**
	 * Test the size and price of the order
	 * 
	 */
	@Test
	public void sizePriceTest() {
		 assertEquals(12, 12);
    	 assertEquals(12L, 12L);
}
	
	/**
	 * Test the instrument that call ric
	 * @return 
	 * 
	 * 
	 */
	@Test
	public void instumentTest() {
		Instrument instrument = new Instrument(new Ric("YES!"));
		assertEquals("Yes", string);
	
	}
	
}

