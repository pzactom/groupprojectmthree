package OrderManager;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import Database.Database;
import OrderClient.NewOrderSingle;
import OrderRouter.Router;
import TradeScreen.TradeScreen;

public class OrderManager {

	private Logger logger = LoggerFactory.getLogger(OrderManager.class);
	public static final int ORDER_STALE_TIME_SECONDS = 30;
	public enum api{newOrderSingle, cancelOrder, bestPrice, newFill, acceptOrder, sliceOrder};
	private HashMap<Integer, Order> orders = new HashMap<Integer, Order>(); // debugger will do this line as it gives
																			// state to the object
	private HashMap<Integer, Order> cancelledOrders = new HashMap<Integer, Order>();
	
	// currently recording the number of new order messages we get. TODO why? use it
	// for more?
	private int id = 0; // debugger will do this line as it gives state to the object
	private Socket[] orderRouters; // debugger will skip these lines as they dissapear at compile time into 'the
									// object'/stack
	private Socket[] clients;
	private Socket trader;
	private Socket liveMarketData;
	public boolean running = true;

	/**
	 * Connect to the given InetSocketAddress and return the socket created. If the connection failed
	 * then null is returned. 
	 * @param location - The location to connect to
	 * @return - Return the connected socket 
	 * @throws InterruptedException
	 */
	private Socket connect(InetSocketAddress location) throws InterruptedException {
		boolean connected = false;
		int tryCounter = 0;
		while (!connected && tryCounter < 600) {
			try {
				Socket s = new Socket(location.getHostName(), location.getPort());
				s.setKeepAlive(true);
				return s;
			} catch (IOException e) {
				Thread.sleep(1000);
				tryCounter++;
			}
		}

		logger.error("Failed to connect to {}", location);
		return null;
	}

	/**
	 * @param args
	 *            the command line arguments
	 * @param orderRouters
	 * @param clients
	 * @param trader
	 * @param liveMarketData
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 */
	public OrderManager(InetSocketAddress[] orderRouters, InetSocketAddress[] clients, InetSocketAddress trader,
			InetSocketAddress liveMarketData) throws IOException, ClassNotFoundException, InterruptedException {
		this.liveMarketData = connect(liveMarketData);
		this.trader=connect(trader);

		//Create the stale order watchdog
		ScheduledExecutorService staleOrderWatchdog = Executors.newSingleThreadScheduledExecutor();
		staleOrderWatchdog.scheduleAtFixedRate(()->{cancelStaleOrders();},
				ORDER_STALE_TIME_SECONDS,
				ORDER_STALE_TIME_SECONDS,
				TimeUnit.SECONDS);

		// for the router connections, copy the input array into our object field.
		// but rather than taking the address we create a socket+ephemeral port and
		// connect it to the address
		this.orderRouters = new Socket[orderRouters.length];
		int i = 0; // need a counter for the the output array
		for (InetSocketAddress location : orderRouters) {
			this.orderRouters[i] = connect(location);
			i++;
		}

		// repeat for the client connections
		this.clients = new Socket[clients.length];
		i = 0;
		for (InetSocketAddress location : clients) {
			this.clients[i] = connect(location);
			i++;
		}
		int clientId, routerId;
		Socket client, router;
		// main loop, wait for a message, then process it
		while (running) {
			
			//we want to use the arrayindex as the clientId, so use traditional for loop instead of foreach
			for(clientId=0;clientId<this.clients.length;clientId++){ //check if we have data on any of the sockets
				client=this.clients[clientId];
				if(0<client.getInputStream().available()){ //if we have part of a message ready to read, assuming this doesn't fragment messages
					handleClientMessage(clientId);
				}
			}
			
			for(routerId=0;routerId<this.orderRouters.length;routerId++){ //check if we have data on any of the sockets
				router=this.orderRouters[routerId];
				if(0<router.getInputStream().available()){ //if we have part of a message ready to read, assuming this doesn't fragment messages
					handleRouterMessage(routerId);
				}
			}
			
			if(0<this.trader.getInputStream().available()){
				handleTraderMessage(this.trader);
			}
			

			if (0 < this.liveMarketData.getInputStream().available()) {
				// OM will receive a response from LiveMarketData
				Order o = null;
				ObjectInputStream is = new ObjectInputStream(this.liveMarketData.getInputStream());
				try {
					o = (Order) is.readObject();
					sendOrderToTrader(o.id, o, TradeScreen.api.price);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
			Thread.sleep(10);
		}
	}
	
	
	private void handleTraderMessage(Socket trader) throws IOException, ClassNotFoundException {
		ObjectInputStream is=new ObjectInputStream(this.trader.getInputStream());
		api method=(api)is.readObject();
		logger.debug("received command from trader to call {}", method);
		switch(method){
			case acceptOrder: acceptOrder(is.readInt());break;
			case sliceOrder:
				int clientID = is.readInt();
				int sliceSize = is.readInt();
				sliceOrder(clientID ,sliceSize);
				break;
			case cancelOrder:
				int orderID = is.readInt();
				cancelOrder(orders.get(orderID));
				break;
			default:
				logger.error("Unknown method received {}", method);
		}
	}
	
	/**
	 * Handle a message sent by the client with the given ID.
	 * @param clientId - The id of the client that send the message
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void handleClientMessage(int clientId) throws IOException, ClassNotFoundException {
		Socket client = clients[clientId];
		ObjectInputStream is=new ObjectInputStream(client.getInputStream()); //create an object inputstream, this is a pretty stupid way of doing it, why not create it once rather than every time around the loop 
		api method=(api)is.readObject();
		logger.debug("received command from client {} to call {}", clientId, method);
		int clientOrderId = 0;
		switch(method){
			case newOrderSingle : 
				clientOrderId = is.readInt();
				NewOrderSingle nos = (NewOrderSingle)is.readObject();
				newOrder(clientId, clientOrderId, nos);
				break;
			case cancelOrder:
				clientOrderId = is.readInt();
				cancelOrder(clientId, clientOrderId);
				break;
			default:
				logger.error("Unkown message type received: {}", method);
				break;
		}
	}
	
	/**
	 * Handle a message passed by the router of the given ID
	 * @param routerId - The id of the router that sent the message
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void handleRouterMessage(int routerId) throws IOException, ClassNotFoundException {
		Socket router = orderRouters[routerId];
		ObjectInputStream is=new ObjectInputStream(router.getInputStream()); //create an object inputstream, this is a pretty stupid way of doing it, why not create it once rather than every time around the loop
		api method=(api)is.readObject();
		int sliceId, orderId;
		switch(method){ //determine the type of message and process it
			case bestPrice:
				orderId=is.readInt();
				sliceId=is.readInt();
				Order slice=orders.get(orderId).slices.get(sliceId);
				slice.bestPrices[routerId]=is.readDouble();
				slice.bestPriceCount+=1;
				logger.debug("Received best price from router {} for order {}, sliceid{}", routerId, orderId,sliceId);
				if(slice.bestPriceCount==slice.bestPrices.length)
					reallyRouteOrder(sliceId, slice);
				break;
			case newFill:
				orderId = is.readInt();
				sliceId = is.readInt();
				int size = is.readInt();
				double price = is.readDouble();
				logger.debug("Received command from router {} to call {}. Orderid: {}, Sliceid: {}, size: {}, price: {}",
						routerId, method, orderId, sliceId, size, price);
				newFill(orderId, sliceId, size, price);
				break;
			default:
				logger.error("Unrecongnised method recied: {}", method);
				break;
		}
	}

	/**
	 * Place a new order and notify trader that the order has been received but not
	 * yet accepted
	 * 
	 * @see #sendOrderToTrader(int, Order, Object)
	 * @param clientId
	 *            - The id of the client
	 * @param clientOrderId
	 *            - The id of the order sent by the client
	 * @param nos
	 *            - New single order
	 * @throws IOException
	 */
	private void newOrder(int clientId, int clientOrderId, NewOrderSingle nos) throws IOException {
		orders.put(id, new Order(id, clientId, clientOrderId, nos.instrument, nos.size, nos.isBuyOrder));
		// send a message to the client with 39=A; //OrdStatus is Fix 39, 'A' is
		// 'Pending New'
		ObjectOutputStream os = new ObjectOutputStream(clients[clientId].getOutputStream());
		// newOrderSingle acknowledgement
		// ClOrdId is 11=
		os.writeObject("11=" + clientOrderId + ";35=A;39=A;");
		os.flush();
		sendOrderToTrader(id, orders.get(id), TradeScreen.api.newOrder);
		// send the new order to the trading screen
		// don't do anything else with the order, as we are simulating high touch orders
		// and so need to wait for the trader to accept the order
		id++;
	}

	/**
	 * Send the order to the trader
	 * @param id - The id of the order
	 * @param o - The order to send
	 * @param method - The type of message being sent
	 * @throws IOException
	 */
	private void sendOrderToTrader(int id, Order o, Object method) throws IOException {
		ObjectOutputStream ost = new ObjectOutputStream(trader.getOutputStream());
		ost.writeObject(method);
		ost.writeInt(id);
		ost.writeObject(o);
		ost.flush();
	}

	/**
	 * Accept the order of given id. And notify the client of the acceptance.
	 * @param id - The id of the order
	 * @throws IOException 
	 */
	public void acceptOrder(int id) throws IOException{
		Order o = null;
		if(isOrderCanceled(id))
			o = cancelledOrders.get(id);
		else
			o = orders.get(id);
		
		if(o.ordStatus == '4') {
			logger.error("Cannot accept order that has been canceled");
			return;
		}
		
		if(o.ordStatus!='A'){ //Pending New
			//TODO error handling
			logger.error("Cannot accept order that has already been accepted: {}", id);
			return;
		}

		// Send that we have accepted the order
		o.ordStatus = '0'; // New
		ObjectOutputStream os = new ObjectOutputStream(clients[o.clientid].getOutputStream());
		// newOrderSingle acknowledgement
		// ClOrdId is 11=
		os.writeObject("11=" + o.ClientOrderID + ";35=A;39=0");
		os.flush();

		//Get the price for the accepted order
		price(id,o);
	}

	/**
	 * Slice the order with the given ID by creating a new slice of the defined size.
	 * @param id - The id of the order to slice
	 * @param sliceSize - The size of new slice
	 * @throws IOException
	 */
	public void sliceOrder(int id, int sliceSize) throws IOException {
		Order o = orders.get(id);
		// slice the order. We have to check this is a valid size.
		// Order has a list of slices, and a list of fills, each slice is a childorder
		// and each fill is associated with either a child order or the original order
		if (sliceSize > o.sizeRemaining() - o.sliceSizes()) {
			logger.error("sliceSize({}) is bigger than remaining size({}) to be filled on the order: ", sliceSize,
					o.sizeRemaining(), id);
			return;
		}

		int sliceId = o.newSlice(sliceSize);
		Order slice = o.slices.get(sliceId);
		// Try to match the new slice internally
		internalCross(id, slice);
		// If slice not matched send to router
		int sizeRemaining = o.slices.get(sliceId).sizeRemaining();
		if (sizeRemaining > 0) {
			routeOrder(id, sliceId, sizeRemaining, slice);
		}
	}

	/**
	 * Iterate through the list of orders and if there are any that have not been
	 * updated in more than {@link #ORDER_STALE_TIME_SECONDS} then they are to be
	 * automatically cancelled.
	 */
	private void cancelStaleOrders() {
		logger.debug("Clearing stale orders");
		long epochTime = LocalDateTime.now().toEpochSecond(ZoneOffset.ofHours(0));
		
		//Clone the orders list to prevent a concurent modification error
		Order[] orderList = new Order[orders.values().size()];
		orders.values().toArray(orderList);
		for(Order order : orderList) {
			if(order.ordStatus == '4' || order.ordStatus == '2') continue;
			if((epochTime - order.lastActive()) > ORDER_STALE_TIME_SECONDS){
				try {
					this.cancelOrder(order);
				} catch (Exception e) {
					logger.error("Failed to cancel clientID: {}, orderID: {} ", order.clientid, order.ClientOrderID);
				}
			}
		}
	}
	
	/**
	 * Has the given order id been canceled
	 * @param id
	 * @return
	 */
	private boolean isOrderCanceled(int id) {
		if(cancelledOrders.get(id) != null) return true;
		return false;
	}

	/**
	 * Cancel the order from the given client ID with the client orderID given. The order will
	 * be cancelled from the order manager and the routers.
	 * @param clientId - The client id who submitted the order to cancel
	 * @param clientOrderId - The ID the client uses to identify the order
	 * @throws IOException
	 */
	private void cancelOrder(int clientId, int clientOrderId) throws IOException {
		Order orderToCancel = null;
		for(Order order : orders.values()) {
			if(order.clientid == clientId && order.ClientOrderID == clientOrderId) {
				orderToCancel = order;
			}
		}
		if(orderToCancel != null)
			cancelOrder(orderToCancel);
		else {
			logger.error("Can not cancel order with clientID: {} and clientOrderID: {} as it could not be found",
					clientId, clientOrderId);
		}
	}
	
	private void sendCancelOrderToRouter(Order order, int routerId) throws IOException {
		ObjectOutputStream ost = new ObjectOutputStream(orderRouters[routerId].getOutputStream());
		ost.writeObject(Router.api.sendCancel);
		ost.writeInt(order.id);
		ost.writeObject(order);
		ost.flush();
	}

	/**
	 * Internal crossing, taking orders we have and comparing them against the given order trying
	 * to fulfill the order as best as possible.
	 * @param id - The id of order
	 * @param o - The order to match with
	 * @throws IOException
	 */
	private void internalCross(int id, Order o) throws IOException {
		for (Map.Entry<Integer, Order> entry : orders.entrySet()) {
			if (entry.getKey().intValue() == id)
				continue;
			Order matchingOrder = entry.getValue();
			if(matchingOrder.ordStatus != '1' || matchingOrder.ordStatus != '0') {
				continue;
			}
			if (o.isBuyOrder() == matchingOrder.isBuyOrder())
				continue;
			if (!(matchingOrder.instrument.equals(o.instrument)
					&& matchingOrder.initialMarketPrice == o.initialMarketPrice))
				continue;
			// TODO add support here and in Order for limit orders
			int sizeBefore = o.sizeRemaining();
			o.cross(matchingOrder);
			// If we made a trade send to trader
			if (sizeBefore != o.sizeRemaining()) {
				sendOrderToTrader(id, o, TradeScreen.api.cross);
			}
		}
	}

	/**
	 * Cancel the given order by sending a cancel command to the router to cancel it there and setting the status to
	 * Cancelled here. The cancel order will be cancelled if its not been completed, if partial the order will be
	 * broken into two orders of the same ID, one with a status complete representing the partially filled portion, and
	 * the other will be cancelled representing the unfilled portion.
	 * @param order The order to cancel
	 */
	private void cancelOrder(Order order) {
		try {
			logger.info("Cancelling order {}, clientOrderID: {}", order.id, order.ClientOrderID);
			//Check that the order is not complete
			if(order.ordStatus == '2') {
				logger.error("Cannot cancel complete order: {}", order.id);
				return;
			}
			
			//Remove the order from the hashmap
			orders.remove(order.id);
			
			//Is the order a partial fill, if so then separate the order into two separate
			//The first the compete section, the second a cancelled order
			if(order.ordStatus == '1') {
				logger.debug("canceling the partial order: {}", order.id);
				Order completeOrder = order.getCompletePortion();
				Order remainingOrder = order.getRemainingOrder();
				order = remainingOrder;
				
				//replace the order
				orders.put(completeOrder.id, completeOrder);
				
				//Send a notification about the complete order
				sendOrderToTrader(order.id, completeOrder, TradeScreen.api.complete);
				ObjectOutputStream ost = new ObjectOutputStream(clients[order.clientid].getOutputStream());
				ost.writeObject("11="+order.clientid+";35=A;39=1;");
			}
			order.ordStatus = '4';
			cancelledOrders.put(order.id, order);
			
			//Send the cancel to the trader and the router
			sendCancelOrderToRouter(order, order.orderRouter);
			sendOrderToTrader(order.id, order, TradeScreen.api.cancel);
			ObjectOutputStream ost = new ObjectOutputStream(clients[order.clientid].getOutputStream());
			ost.writeObject("11="+order.clientid+";35=A;39=4;");
			
		} catch ( Exception e) {
			logger.error("An exception occured when sending a cancel request", e);
		}
	}


	/**
	 * Fulfil a part of the given order
	 * 
	 * @param id
	 *            - The id of the order
	 * @param sliceId
	 *            - The slide id of the order
	 * @param size
	 *            - The amount of the order being fulfilled
	 * @param price
	 *            - The price the order is being fulfilled at
	 * @throws IOException
	 */
	void newFill(int id, int sliceId, int size, double price) throws IOException {
		Order o = orders.get(id);
		if (o.slices.get(sliceId) != null)
			o.slices.get(sliceId).createFill(size, price);
		else
			o.createFill(size, price);
		// When order complete write to database
		if (o.sizeRemaining() == 0) {
			Database.write(o);
		}
		// Send message to trader
		sendOrderToTrader(id, o, TradeScreen.api.fill);
	}

	/**
	 * Send the order to the routers, asking each for the best price they can offer.
	 * 
	 * @param id
	 *            - The order id
	 * @param sliceId
	 *            - The id of the slice in the order
	 * @param size
	 *            - The size of the order - not used inside
	 * @param order
	 *            - The order
	 * @throws IOException
	 */
	private void routeOrder(int id, int sliceId, int size, Order order) throws IOException {
		for (Socket r : orderRouters) {
			ObjectOutputStream os = new ObjectOutputStream(r.getOutputStream());
			os.writeObject(Router.api.priceAtSize);
			os.writeInt(id);
			os.writeInt(sliceId);
			os.writeObject(order.instrument);
			os.writeInt(order.sizeRemaining());
			os.flush();
		}
		// need to wait for these prices to come back before routing
		order.bestPrices = new double[orderRouters.length];
		order.bestPriceCount = 0;
	}

	/**
	 * Send order to the router, find the lowest price of seller. This method
	 * assumes buying Diagram = internalCross->route
	 * 
	 * @param sliceId
	 *            Index of order slice
	 * @param o
	 *            - Order being send to router
	 * @throws IOException
	 */
	private void reallyRouteOrder(int sliceId,Order o) throws IOException{
		int index = 0;
		
		if(o.isBuyOrder()) {
			//Best price index
			double min=o.bestPrices[0];
			for(int i=1;i<o.bestPrices.length;i++){
				if(min>o.bestPrices[i]){
					index=i;
					min=o.bestPrices[i];
				}
			}
		} else {
			//Get the best buying price
			double max = o.bestPrices[0];
			for(int i = 1; i < o.bestPrices.length; i++) {
				if(max < o.bestPrices[i]) {
					index = i;
					max = o.bestPrices[i];
				}
			}
		}
		
		ObjectOutputStream os=new ObjectOutputStream(orderRouters[index].getOutputStream());
		os.writeObject(Router.api.routeOrder);
		os.writeInt(o.id);
		os.writeInt(sliceId);
		os.writeInt(o.sizeRemaining());
		os.writeObject(o.instrument);
		os.flush();
	}

	/**
	 * set the initial price of the given order
	 * @param id - the order manager order ID
	 * @param o - The order to find the price for
	 * @throws IOException
	 */
	private void price(int id, Order o) throws IOException {
		// OM sends an order to LiveMarketData for the setprice function
		ObjectOutputStream os = new ObjectOutputStream(this.liveMarketData.getOutputStream());
		os.writeObject(o);
		os.flush();
	}
}
