package OrderManager;

import java.io.Serializable;

/**
 * Represents the amount of an order filled.
 * Containes the size of the fill and the price which the will was carried out at.
 */
public class Fill implements Serializable {
	int size;
	double price;

	/**
	 * Create a new fill
	 * @param size - The size of the oder being filled 
	 * @param price - The price at which the fill is taking place
	 */
	Fill(int size, double price) {
		this.size = size;
		this.price = price;
	}
}