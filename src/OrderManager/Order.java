package OrderManager;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import Ref.Instrument;

public class Order implements Serializable {
	
	public int id = 0;
	short orderRouter;
	public int ClientOrderID; // TODO refactor to lowercase Case
	double[] bestPrices;
	int bestPriceCount;
	private long lastActivtyEpoch = 0;
	
	/** The full size of the order before splits and fills */
	public int size;
	ArrayList<Order> slices;
	ArrayList<Fill> fills;
	public int clientid;
	public Instrument instrument;
	public double initialMarketPrice;
	public char ordStatus = 'A'; // OrdStatus is Fix 39, 'A' is 'Pending New'
	private Logger logger = LoggerFactory.getLogger(Order.class);
	private boolean isBuyOrder;
	
	
	/**
	 * Create a new order object to represent a received order/slice.
	 * @param clientId - The ID by the client can be uniquely identified
	 * @param ClientOrderID - The id that the client uses to reference the order.  
	 * @param instrument - The instrument the order is for
	 * @param size - The size of the order
	 * @param buyOrder - True of this order is to buy an instrument, false to sell
	 */
	public Order(int managerID, int clientId, int ClientOrderID, Instrument instrument, int size, boolean buyOrder) {
		this.id = managerID;
		this.ClientOrderID = ClientOrderID;
		this.size = size;
		this.clientid = clientId;
		this.instrument = instrument;
		fills = new ArrayList<Fill>();
		slices = new ArrayList<Order>();
		this.isBuyOrder = buyOrder;
		updateActivityTime();
	}
	
	/**
	 * Count and return the portion of this order that has been separated into sliced.
	 * @return The sum of the slices size in this order
	 */
	public int sliceSizes() {
		int totalSizeOfSlices = 0;
		for (Order c : slices)
			totalSizeOfSlices += c.size;
		return totalSizeOfSlices;
	}

	/**
	 * Create a new slice from this order, the new slice will have the same id, client id, instrument type and
	 * initial market price.
	 * @param sliceSize The size of the new slice, this must be less than the current remaining.
	 * @return The slide id of the newly created slice.
	 */
	public int newSlice(int sliceSize) {
		Order newSlice = new Order(this.id, clientid, ClientOrderID, instrument, sliceSize, this.isBuyOrder);
		newSlice.initialMarketPrice = this.initialMarketPrice;
		slices.add(newSlice);
		logger.info("Order {} has been split by {}", this.id, sliceSize);
		updateActivityTime();
		return slices.size() - 1;
	}
	
	public boolean isBuyOrder() {
		return this.isBuyOrder;
	}
	

	/**
	 * Get the portion of the order that has been fulfilled by counting the fills on this order and its slices.
	 * @return The portion of the order that has been filled.
	 */
	public int sizeFilled() {
		int filledSoFar = 0;
		for (Fill f : fills) {
			filledSoFar += f.size;
		}
		for (Order c : slices) {
			filledSoFar += c.sizeFilled();
		}
		return filledSoFar;
	}

	/**
	 * The size of the order that has not yet been filled. Includes the slices
	 * @return The portion of the order remaining to be filled
	 */
	public int sizeRemaining() {
		return size - sizeFilled();
	}

	/**
	 * The full size of the order not counting any fills.
	 * @return The original size of the order
	 */
	public int sizeTotal() {
		return this.size;
	}
	
	/**
	 * Get the size of the unfulfilled portion of this orders slices.
	 * @return The total remaining in the slices
	 */
	public int sliceRemaining() {
		int remaining = 0;
		for(Order slice : slices) {
			remaining += slice.sizeRemaining();
		}
		return remaining;
	}

	/**
	 * The average price of all the fills for this order.
	 * This is the non weighted average.
	 * @return Non weighted average price
	 */
	float price() {
		float sum = 0;
		int divider = fills.size();
		for (Fill fill : fills) {
			sum += fill.price;
		}
		//Count the price of the fills from the slices from this order
		for (Order slice : slices) {
			divider += slice.fills.size();
			for(Fill fill : slice.fills) {
				sum += fill.price;
			}
		}
		
		return sum / divider;
	}
	
	/**
	 * Get the epoch time of the last time this order had any action carried out it.
	 * The larger the time, the more recent the change
	 * @return The epoch time in seconds since the last activity in this order. 
	 */
	public long lastActive() {
		long mostRecentActivty = this.lastActivtyEpoch;
		
		//Get the most recent change 
		for(Order slice : slices) {
			if(slice.lastActive() > mostRecentActivty)
				mostRecentActivty = slice.lastActive();
		}
		
		return mostRecentActivty;		
	}

	/**
	 * Create a fill in which we fulfil part of the current order.
	 * Sets internal order status depending on unfulfilled order:
	 * 	2 - complete
	 *  1 - partial fill
	 * 
	 * @param size The portion of the order being filled
	 * @param price The price of the fill 
	 */
	void createFill(int size, double price) {
		fills.add(new Fill(size, price));
		logger.info("Order {} filled by {} at {}", this.id, size, price);
		if (sizeRemaining() == 0) {
			ordStatus = '2';
		} else {
			ordStatus = '1';
		}
		updateActivityTime();
	}

	/**
	 * Cross the given order with this order. This method assumes the order given is able to be matched with this
	 * order.
	 * @param matchingOrder The order to match this order with
	 */
	public void cross(Order matchingOrder) {
		if(this.isBuyOrder == matchingOrder.isBuyOrder) return;
		//Try to match the slices from this order first.
		for (Order slice : slices) {
			if (slice.sizeRemaining() == 0)
				continue;

			//Try to match the slice from this order with a slice from the other order
			for (Order matchingSlice : matchingOrder.slices) {
				if(slice.sizeRemaining() == 0)
					break; //Don't match if there are no more remaining
				slice.match(matchingSlice);
			}
			
			//If the slice is still unfulfilled then try to fill it from the matching orders non sliced section
			//Get the remaining size of the non sliced remaining
			int mParent = matchingOrder.sizeRemaining() - matchingOrder.sliceRemaining();
			if (mParent > 0) {
				slice.match(matchingOrder);
			}
			
		
			
			// no point continuing if we didn't fill this slice, as we must already have fully filled the matchingOrder
			if (slice.sizeRemaining() > 0)
				break;
		}

		//Match this order against the order passed. Assume at this point the slices are fulfilled.
		if (sizeRemaining() > 0) {
			for (Order matchingSlice : matchingOrder.slices) {				
				if(this.sizeRemaining() == 0)
					break;
				this.match(matchingSlice);
			}
			
			int mParent = matchingOrder.sizeRemaining() - matchingOrder.sliceRemaining();
			if (mParent > 0) {
				this.match(matchingOrder);
			}
		}
	}
	
	/**
	 * Match the given order with this order, tried to match the order as if it was a slice. Slices that are part
	 * of the passed orders are not considered.
	 * @param orderM - The order to match this order against
	 */
	private void match(Order orderM) {
		int thisRemaining = this.sizeRemaining() - this.sliceRemaining();
		int otherRemaining = orderM.sizeRemaining() - this.sliceRemaining();
		
		//If one of the orders are filled then don't try to match
		if(thisRemaining == 0 || otherRemaining == 0) return;
		logger.debug("Match taking place between order {} and order {}", this.id, orderM.id);
		//Start to match
		if(thisRemaining < otherRemaining) {
			this.createFill(thisRemaining, initialMarketPrice);
			orderM.createFill(thisRemaining, initialMarketPrice);
		} else {
			this.createFill(otherRemaining, initialMarketPrice);
			orderM.createFill(otherRemaining, initialMarketPrice);
		}
		updateActivityTime();
	}
	
	/**
	 * Update the time the order was last updated in any way.
	 */
	private void updateActivityTime() {
		this.lastActivtyEpoch = LocalDateTime.now().toEpochSecond(ZoneOffset.ofHours(0));
	}

	//TODO ELLI
	// TODO implement cancel()
	void cancel() {
		// state=cancelled
	}

	/**
	 * Get the complete portion of this order containing all of the complete fills. The split
	 * Information is not recorded. The purpose if this is to allow the separation of complete and
	 * remaining sections of the order when cancelling a partial fill
	 * @return The complete portion of this order
	 */
	public Order getCompletePortion() {
		Order completeOrder = new Order(id, clientid, ClientOrderID, instrument, sizeFilled(), isBuyOrder);
		completeOrder.ordStatus = '2';
		completeOrder.fills.addAll(this.fills);
		for(Order slice : this.slices) {
			completeOrder.fills.addAll(slice.fills);
		}
		return completeOrder;
	}
	
	/**
	 * Get the unfulfilled section of this order. The split information is not recorded. The purpose of this
	 * is to allow for the separation of complete and remaining section of the order when cancelling a
	 * partial fill.
	 * @return The partial of the order that is remaining
	 */
	public Order getRemainingOrder() {
		Order remaining = new Order(id, clientid, ClientOrderID, instrument, sizeRemaining(), isBuyOrder);
		remaining.ordStatus = '4';
		return remaining;
	}


}



