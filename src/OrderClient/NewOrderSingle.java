package OrderClient;

import java.io.Serializable;

import OrderManager.Order;
import Ref.Instrument;

public class NewOrderSingle implements Serializable{
	public int size;
	public float price;
	public boolean isBuyOrder;
	public Instrument instrument;
	public NewOrderSingle(int size,float price,Instrument instrument, boolean isBuyOrder){
		this.size=size;
		this.price=price;
		this.instrument=instrument;
		this.isBuyOrder = isBuyOrder;
	}

}