package Database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//TODO figure out how to make this abstract or an interface, but want the method to be static
public class Database{
	private static Logger logger = LoggerFactory.getLogger(Database.class);
	public static void write(Object o){
		logger.info("{}", o);
	}
}